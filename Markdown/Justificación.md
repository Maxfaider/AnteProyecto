#**2.       Justificación de la investigación**

Se ha de mostrado que el mantenimiento del software en sistemas heredados, es una de la etapa más prolongada y la que más costos genera, además sin el conocimiento del sistema existente y sin una documentación apropiada (documentación inexistente, escaza o desactualizada), lo hacen una tarea tediosa y costosa. Para resolver este problema, uno puede recuperar su documentación usando como principal artefacto el código fuente. 

##**2.1       Justificación de Carácter Práctica**

Esta investigación propone una metodología basada en la ingeniería inversa como una solución para entender el sistema existente, a través de la recuperación de su documentación funcional y las especificaciones de diseño de sistemas desarrollados con el lenguaje COBOL, por medio de uno de los estándares disponible públicamente del Object Management Group (OMG), que es el Metamodelo para el descubrimiento de conocimiento (KDM), la cual define un vocabulario común de conocimiento, independientemente del lenguaje de programación de implementación y la plataforma de tiempo de ejecución, permitiendo la interoperabilidad entre herramientas de Modernización.

La representación explicita del conocimiento del código fuente a partir KDM, permite guiar el proceso de desarrollo, verificación, reingeniería, evolución o integración de sistemas heredados con el lenguaje de programación COBOL, además de reducir costes y tiempos de mantenimiento de sistemas basados en este.

##**2.2       Justificación de Carácter Metodológico**

La investigación es importante porque se propone un estudio de diferentes metodologías relacionadas a la ingeniería inversa. la visión es incluir ingeniería inversa como parte de lo normal en desarrollo y evolución del sistema de software.  En puntos regulares durante el ciclo de desarrollo, el código se revertiría para redescubrir su diseño, para que la documentación se pueda actualizar. Esto ayudaría a evitar la situación típica en la que la información detallada sobre un sistema de software, como su arquitectura, restricciones de diseño, se encuentra solo en la memoria de su desarrollador.

El problema del mantenimiento del software no se puede disipar con alguna técnica inteligente, [7] argumenta que "el código de reingeniería para crear un sistema que no necesite ingeniería inversa en el futuro, es actualmente inalcanzable".

### **2.2.1	¿Porque KDM?**

KDM es la representación independiente del lenguaje y la plataforma. Su mecanismo de extensibilidad permite la adición de conocimiento específico de dominio, aplicación e implementación.

KDM representa sistemas completos de software empresarial, desde elementos estructurales hasta de comportamiento de los sistemas de software existentes.

KDM facilita el análisis de los sistemas de software existentes. Los pasos del proceso de extracción de conocimiento se pueden realizar con herramientas y pueden involucrar al analista.

### **2.2.2	¿Porque COBOL?**

Cobol no es un lenguaje obsoleto y está muy lejos de eso, Si bien una gran cantidad de software que se ha escrito ya no se utiliza, una cantidad considerable ha sobrevivido durante décadas y continúa funcionando en la economía global. Según [3] “La realidad de la situación es que el 70% del código fuente en el mundo entero está escrito en COBOL” (2005). “Uno se vería en apuros hoy en día para obtener una educación experta en lenguajes de programación antiguos como COBOL, PL / I y FORTRAN. Para agravar la situación, gran parte del código heredado está mal documentado”. Los programas COBOL se usan globalmente en instituciones gubernamentales y agencias militares, en empresas comerciales y en sistemas operativos como z / OS® de IBM, Windows de Microsoft y familias POSIX (Unix / Linux, etc.). En 1997, el Grupo Gartner informó que “el 80% de los negocios del mundo funcionaba en COBOL con más de 200 mil millones de líneas de código y con un estimado de 5 mil millones de líneas de código nuevo anualmente".

Dado que es un riesgo reemplazar miles de millones de líneas de códigos heredados, una alternativa razonable ha sido mantener y evolucionar el código, con la ayuda de los conceptos que se encuentran en la ingeniería inversa.


## **2.3       Importancia de la investigación**

Como se ha mencionado anteriormente el mantenimiento software es una de las más costosas y prologadas etapas, aún más, si no se cuenta con una documentación adecuada del sistema. Con esta investigación, se pretende brindar una metodología para obtener las vistas arquitectónicas a partir del código fuente de un sistema, ya construido en COBOL.
