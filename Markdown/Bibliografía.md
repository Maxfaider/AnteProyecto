#**Bibliografía**

[1] E. B Ghizlane, B. B. Alvine, V. Stéphane, M. Hafedh, “Reconstructing Architectural Views from Legacy Systems”, 19th, Working Conference on Reverse Engineering, 2012.

[2] T. Paolo, P. Alessandra, “Reverse Engineering of Object Oriented Code”, Springer Science + Business Media Inc, 2005.

[3] M. R. Ali, "Why teach reverse engineering?" ACM SIGSOFT SEN, v.30, n.4, pp. 1-4, Jul 2005.

[4] Dr. Nikolai Mansurov, “Knowledge Discovery Meta-model: Tutorial”, 24th, ADM workshop Washington DC, October 2005.

[5] K. e. Kenneth, K. e. Julie, “ANÁLISIS Y DISEÑO DE SISTEMAS”, 8th, Rutgers University - School of Business–Camden, New Jersey, 2011.

[6] Joost Visser, “Building Mantenible Software: Ten Guidelines For Future-Proof Code”, First Edition, Software Improvement Group, 2016.

[7] B.W Weide, W D. Heym, J. E. Hollingsworth, "Reverse engineering of legacy code exposed", inProc.  17th Int. Conf.Software Engineering, Seattle, Washington, WA, 1995, pp. 327-331.

[8] Ian Sommerville, “Ingeniería del software”, The Public Relations Handbook,7th edition, 2005 Madrid.

[9] Roger S. Pressman, “Ingeniería del software un enfoque práctico”, McGraw-Hill, 6th edition, 2005.

[10] Hanna M., “Maintenance: Burden Begging for a Remedy”. Datamation, abril 1993.

[11] Regis Vogel, “Fifth European Conference on Model-Driven Architecture Foundations and Applications: Proceedings of the Tools and Consultancy Track”, Foundations and Applications (ECMDA-FA 2009), University of Twente, june 23 - 26 2009.

[12] Hongji yang, William C. Chu, “Acquisition of Entity Relationship Models for Maintenance-Dealing with Data Intensive Programs in a Transformation System”, Journal of Information Science and Engineering 15, April 18, 1997.

[13] B. Franck, R. Jean-Luc, “COBOL Software Modernization”, Computer Engineering series, 2015.

[14] R. Baeza-Yates, “Modern Information Retrieval”, ACM Press - Addison Wesley, 1999. URL <http://people.ischool.berkeley.edu/~hearst/irbook/>

[15] D. M. Francisco, T. C. Javier, V. M. Antonio, “Desarrollo de software dirigido por modelos”, v.3.0, FUOC. Fundación para la Universitat Oberta de Catalunya.

[16] R. V. Jesús, Dir. G. M. Jesús J., “Ingeniería de Modelos con MDA”, Estudio comparativo entre OptimalJ y ARCStyler, pp. 15-22, junio 2004.

[17] J Klas, “Recuperación de Información sobre Modelos de Dominio”, 38 JAIIO, 2009. URL <http://www.scribd.com/doc/19217222/Recuperacion-de-Informacion-sobre-Modelos-de-Dominio>