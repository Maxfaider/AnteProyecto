# **3.       Objetivos**

## **3.1       Objetivos Generales**

- Aplicación de la ingeniería inversa para obtener modelos a alto nivel a partir del código fuente COBOL.
- Representar modelos independientes del lenguaje a través de KDM
- Garantizar la interoperabilidad entre las herramientas que soporten KDM.

## **3.2       Objetivos Específicos**

* Obtener la documentación funcional y especificación del sistema, para guiar los procesos de desarrollo, reingeniería, evolución o integración.
* Reducir los costes y tiempo de mantenimiento de sistemas desarrollados en COBOL.
* Validar el sistema y su diseño.
* Garantizar la interoperabilidad entre las herramientas que soporten KDM.

