#**4.       Resultados Esperados**

El desarrollo de la investigación, generará conocimiento que ayudará al análisis y al entendimiento de un sistema, identificado sus componentes actuales y las dependencias que existen entre ellos para crear abstracciones de dicho sistema e información de su diseño. Esto contribuirá guiar el proceso de desarrollo, verificación, reingeniería, evolución o integración de sistemas heredados con el lenguaje de programación COBOL, además de reducir el esfuerzo requerido para llevar a cabo tareas de mantenimiento.

Es aplicable a sistema con las siguientes características: documentación inexistente o totalmente obsoleta; programación en bloques de códigos muy grandes y/o sin estructurar; el sistema cubre gran parte de los requisitos esperados; el sistema está sujeto a cambios frecuentes, que pueden afectar su diseño.

La redocumentación, sistemas con una mejor documentación y menos complejidad, y ajustados a las practicas y estándares de la ingeniería de software moderna.