# **1. Planteamiento del Problema**

Hoy en día, la gran mayoría de las empresas se preocupan por mantener un alto nivel competitivo y satisfacer los requerimientos cambiantes del mercado. Lo anterior implica que los sistemas de información designados a apoyar la gestión de dichas organizaciones, se ven afectados por el cambio. Estas se ven obligadas a cambiar cuando surgen nuevos requerimientos, cuando se desea incorporar una nueva tecnología, la supresión de componentes que han dejado de ser útiles debido a cambios en las políticas de dominio, entre otras cosas. Este capítulo se divide en dos partes principales: Fundamentación del problema (Parte I), Descripción de la realidad (Parte II), Importancia de la investigación (Parte III).

La Parte I, hace énfasis en el impacto del mantenimiento, una vez instalado el sistema hay que darle mantenimiento, lo cual implica que tal vez haya que realizar modificaciones en el sistema para mantenerlo actualizado o ajustado a los requerimientos de la organización, por otro lado, tenemos la escasez de profesionales especializados en COBOL.

La Parte II, se enumeran una serie de problemas que hace difícil mantener los sistemas software, entre ellas podemos mencionar la falta de documentación, que permita a los participantes del proyecto construir aplicaciones mantenibles. 

La Parte III, hace énfasis a partes relevantes de la problemática y se introduce la solución propuesta.

## **1.1	Mantenimiento de software**

El mantenimiento de software es una de las actividades mas comunes en la ingeniería de software, y es el proceso de mejora o optimización de los procesos generados, así como también adición de nuevas funcionalidades y corrección de defectos. Este puede consumir a mas del 60% de las inversiones efectuadas por una organización, entender y adaptarse a lo que otros hicieron no es una tarea sencilla, se han ido cambiando los equipos de desarrollo. Ahora, no es difícil prever que sin el conocimiento del sistema existente y sin una documentación apropiada, este proceso de comprensión se torna aún más complejo cuando se está trabajando con programas escritos hace más de 15 o 20 años, haciendo que los costos se incrementen de forma considerable. Este último pertenece a los llamados sistemas heredados, que tienen características como: fueron desarrollados hace mucho tiempo; documentación escasa, desactualizada o inexistente; y se siguen usando en la actualidad, por lo que normalmente usan tecnología acusada de no reciente, u obsoleta. ¿Acaso el software queda obsoleto, solamente porque no se ha construido con tecnologías nuevas?

Múltiples autores señalan que el mantenimiento es la parte más costosa del ciclo de vida del software. Estadísticamente está comprobado que el coste del mantenimiento de un producto software a lo largo de toda su vida útil supone mas del doble que los costes de su desarrollo.

​                                                                                                    ![costo-del-ciclo-de-vida-software](img/costo-del-ciclo-de-vida-software.png)

​                            *Figura 1. Distribución del costo del ciclo de vida software (Rock-Evans y Hales, 1990).*

Podemos ver como esta distribuido el costo durante el ciclo de vida del sistema, una actividad que tiene una gran repercusión económica, temporal y de recursos. De hecho, según PRESSMAN (2002), “No es raro que una organización de software emplee entre 60 y 70 por ciento de todos sus recursos en mantenimiento del software. El reto del mantenimiento del software comienza cuando, se enfrenta con una creciente lista de corrección de errores, peticiones de adaptación y mejoras categóricas que deben planearse, calendarizarse” (p. 656). Según SOMMERVILLE (2004), “La mayor parte del presupuesto se dedica a mantener, los sistemas existentes y no debería sorprender que el 90% de los costes del software sean costes de evolución” (p. 448).

¿Cuáles son las causas del alto coste de mantenimiento software?, Osborne y Chikofsky [Osb90] proporcionan una respuesta parcial:

“Mucho del software del que dependemos en la actualidad tiene en promedio una antigüedad de 10 a 15 años. Aun cuando dichos programas se crearon usando las mejores técnicas de diseño y codificación conocidas en la época [y muchas no lo fueron], se produjeron cuando el tamaño del programa y el espacio de almacenamiento eran las preocupaciones principales. Luego migraron a nuevas plataformas, se ajustaron para cambios en máquina y tecnología de sistema operativo, y aumentaron para satisfacer las necesidades de los nuevos usuarios, todo sin suficiente preocupación por la arquitectura global. El resultado es estructuras pobremente diseñadas, pobre codificación, pobre lógica y pobre documentación de los sistemas de software que ahora debemos seguir usando...”. 

## **1.2	Descripción de la Realidad**

El mantenimiento y evolución de sistemas software se ha vuelto una tarea compleja que involucra un gran numero de recursos (tanto talento como material). En la actualidad existen una serie de problemas que hace difícil mantener los sistemas software.

### **1.1.1	La falta de documentación**

El problema que se presenta en múltiples ocasiones cuando el nuevo mantenedor, se encuentra con un producto software con poca o ninguna documentación, o si lo están, sus especificaciones no reflejan los cambios de requerimientos que se dieron a través de los años, esto es debido a la cantidad de cambios continuos y acumulativos sufridos por el sistema lo cual incrementa considerablemente su complejidad, por lo que con el paso del tiempo estos artefactos quedan rápidamente obsoletos al no ser representaciones descriptivas fieles a la forma en que realmente el sistema está construido, se requiere un proceso de reconstrucción de la arquitectura del sistema existente antes iniciar cualquier acción de extensión.

### **1.1.2	 Reescribir código**

La idea de que el nuevo código es mejor que el viejo puede resultar no tan factible. El viejo código ha sido usado, ha sido probado; muchos errores se han encontrado y corregido. Cuando desechas código y empiezas desde cero, estas desechando todo ese conocimiento. Todos aquellos arreglos de errores, años de trajo de programación.

La problemática asociada a ellos, es su evolución, básicamente porque el nuevo sistema deberá cubrir no sólo los nuevos requisitos de la organización sino también los que cubría el sistema heredado. Por lo tanto, no existe una forma directa de especificar un nuevo sistema que sea funcionalmente idéntico al que ya se utiliza. Desechar los sistemas heredados y reemplazados con software moderno conduce a riesgos de negocios significantes. Hay que considerar que muchos de estos sistemas son de naturaleza crítica, e inclusive algunos de ellos funcionan las 24 horas del día. Esto quiere decir, que la detención de dichos sistemas puede traer grandes pérdidas en dinero.

### **1.1.3	La integración**

Las integraciones de los sistemas heredados con otros nuevos sistemas de información más recientes de la propia empresa suponen un gran esfuerzo debido a que las interfaces de comunicación no están bien definidas o son tecnológicamente obsoletas

## **1.3**	Lenguaje Común Orientado a Negocios (COBOL)

Estos sistemas generalmente fueron construidos en COBOL, por lo cual el desafío es aún mayor, teniendo en cuenta que con el pasar el tiempo la cantidad de expertos en la implementación de dicha tecnología ha decaído considerablemente, por lo que, lidiar con código fuente desconocido y poco familiar, que generalmente está construido por personas que no están disponibles para ser consultadas y, en su construcción no han utilizado métodos y técnicas modernas de programación.
COBOL sobrevive como lenguaje de programación porque hay demasiado código legado cuya reescritura es costosa tanto en tiempo y dinero.
